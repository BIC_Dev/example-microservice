package viewmodels

// ErrorResponse struct for a generic error
type ErrorResponse struct {
	Error   string `json:"error"`
	Message string `json:"message"`
}
