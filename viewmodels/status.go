package viewmodels

// GetStatusResponse struct
type GetStatusResponse struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}
