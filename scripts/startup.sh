export ENVIRONMENT=$(echo ${ENV_VARS} | jq -r '.ENVIRONMENT')
export LOG_LEVEL=$(echo ${ENV_VARS} | jq -r '.LOG_LEVEL')
export BASE_PATH=$(echo ${ENV_VARS} | jq -r '.BASE_PATH')
export LISTENER_PORT=$(echo ${ENV_VARS} | jq -r '.LISTENER_PORT')

export SERVICE_TOKEN=$(echo ${SECRETS} | jq -r '.SERVICE_TOKEN')

/main