docker build -t example-microservice .
docker run -e "ENV_VARS=$(<./scripts/env-vars.json)" -e "SECRETS=$(<./scripts/secrets.json)" -p 8080:8080 example-microservice