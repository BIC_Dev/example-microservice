package controllers

// ControllerErr struct
type ControllerErr struct {
	Err error
	Msg string
}

// Error returns the string representation of the error
func (c *ControllerErr) Error() string {
	return c.Err.Error()
}

// Message returns the contextual message for the error
func (c *ControllerErr) Message() string {
	return c.Msg
}
