package controllers

import (
	"net/http"

	"gitlab.com/BIC_Dev/example-microservice/viewmodels"
	"go.uber.org/zap"
)

// GetStatus responds with the availability status of this service
func (c *Controller) GetStatus(w http.ResponseWriter, r *http.Request) {
	err := SendJSONResponse(w, viewmodels.GetStatusResponse{
		Status:  "OK",
		Message: "Service is available",
	}, map[string]string{}, http.StatusOK)

	if err != nil {
		c.Logger.Error("Failed to send GET status response", zap.Error(err.Err))
	}

	return
}
