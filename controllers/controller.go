package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/BIC_Dev/example-microservice/configs"
	"go.uber.org/zap"
)

// Controller struct for dependency injection into controllers
type Controller struct {
	Config       *configs.Config
	Logger       *zap.Logger
	ServiceToken string
}

// SendJSONResponse sends a JSON response to the client
func SendJSONResponse(w http.ResponseWriter, body interface{}, headers map[string]string, status int) *ControllerErr {
	for key, val := range headers {
		w.Header().Set(key, val)
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	err := json.NewEncoder(w).Encode(body)

	if err != nil {
		return &ControllerErr{
			Err: err,
			Msg: "Failed to encode JSON response body",
		}
	}

	return nil
}
