package main

import (
	"log"
	"os"
	"strconv"

	"gitlab.com/BIC_Dev/example-microservice/configs"
	"gitlab.com/BIC_Dev/example-microservice/controllers"
	"gitlab.com/BIC_Dev/example-microservice/routes"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// Main func
func main() {
	env, envExist := os.LookupEnv("ENVIRONMENT")
	serviceToken, stExist := os.LookupEnv("SERVICE_TOKEN")
	listenerPort, lpExist := os.LookupEnv("LISTENER_PORT")
	logLevel, llExist := os.LookupEnv("LOG_LEVEL")
	logLevelInt, lliErr := strconv.Atoi(logLevel)
	basePath, bpExist := os.LookupEnv("BASE_PATH")

	if !envExist && env != "" {
		log.Fatal("Missing ENVIRONMENT environment variable")
	}

	if !stExist && serviceToken != "" {
		log.Fatal("Missing SERVICE_TOKEN secret environment variable")
	}

	if !lpExist && listenerPort != "" {
		log.Fatal("Missing LISTENER_PORT environment variable")
	}

	if !llExist && logLevel != "" {
		log.Fatal("Missing LOG_LEVEL environment variable")
	}

	if lliErr != nil {
		log.Fatalf("Failed to convert LOG_LEVEL to int: %s", lliErr.Error())
	}

	if !bpExist && basePath != "" {
		log.Fatal("Missing BASE_PATH environment variable")
	}

	config, cErr := configs.GetConfig(env)

	if cErr != nil {
		log.Fatalf("%s: %s", cErr.Message(), cErr.Error())
	}

	logger, lErr := InitLogger(logLevelInt)

	if lErr != nil {
		log.Fatalf("Failed to build zap logger: %s", lErr.Error())
	}

	controller := controllers.Controller{
		Config:       &config,
		Logger:       logger,
		ServiceToken: serviceToken,
	}

	router := routes.GetRouter()
	routes.AddRoutes(router, &controller, basePath)

	logger.Info("Starting MUX listener", zap.String("listener_port", listenerPort))
	routes.StartListener(router, listenerPort)
}

// InitLogger intitializes the zap logger
func InitLogger(logLevel int) (*zap.Logger, error) {
	cfg := zap.Config{
		Encoding:         "json",
		Level:            zap.NewAtomicLevelAt(zapcore.InfoLevel),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey: "message",

			LevelKey:    "level",
			EncodeLevel: zapcore.CapitalLevelEncoder,

			TimeKey:    "time",
			EncodeTime: zapcore.ISO8601TimeEncoder,

			CallerKey:    "caller",
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}

	logger, err := cfg.Build()

	if err != nil {
		return nil, err
	}

	return logger, nil
}
