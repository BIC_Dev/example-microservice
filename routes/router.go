package routes

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/BIC_Dev/example-microservice/controllers"
	"gitlab.com/BIC_Dev/example-microservice/viewmodels"
	"go.uber.org/zap"
)

// Middleware to handle functionality pre-controller
type Middleware struct {
	Log          *zap.Logger
	ServiceToken string
	BasePath     string
}

// GetRouter creates and returns a router
func GetRouter() *mux.Router {
	return mux.NewRouter().StrictSlash(true)
}

// AddRoutes adds all necessary routes to the router
func AddRoutes(router *mux.Router, c *controllers.Controller, basePath string) {
	middleware := Middleware{
		Log:          c.Logger,
		ServiceToken: c.ServiceToken,
		BasePath:     basePath,
	}

	router.HandleFunc(basePath+"/status", c.GetStatus).Methods("GET")

	router.Use(middleware.loggingMiddleware)
	router.Use(middleware.authenticationMiddleware)
}

// StartListener starts the HTTP listener
func StartListener(router *mux.Router, port string) {
	log.Fatal(http.ListenAndServe(":"+port, router))
}

func (m *Middleware) loggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		m.Log.Info("Handling request", zap.String("request_uri", r.RequestURI))
		next.ServeHTTP(w, r)
	})
}

func (m *Middleware) authenticationMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		stHeader := r.Header.Get("Service-Token")

		switch r.RequestURI {
		case m.BasePath + "/status":
			next.ServeHTTP(w, r)
			return
		}

		if stHeader != m.ServiceToken {
			err := controllers.SendJSONResponse(w, viewmodels.ErrorResponse{
				Error:   "Unauthorized",
				Message: "The request has not been authorized to use this service",
			}, map[string]string{}, http.StatusUnauthorized)

			if err != nil {
				m.Log.Error(err.Message(), zap.Error(err.Err))
			}

			return
		}

		next.ServeHTTP(w, r)
		return
	})
}
