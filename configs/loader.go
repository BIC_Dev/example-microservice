package configs

import (
	"os"

	"gopkg.in/yaml.v2"
)

// Config represents the structure of the config file
type Config struct {
	SomeConfVar string `yaml:"SOME_CONF_VAR"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(env string) (Config, *ConfigErr) {
	var config Config

	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		return config, &ConfigErr{
			Err: err,
			Msg: "Missing config file at path: " + configFile,
		}
	}

	defer f.Close()

	decoder := yaml.NewDecoder(f)
	dErr := decoder.Decode(&config)

	if dErr != nil {
		return config, &ConfigErr{
			Err: dErr,
			Msg: "Could not parse config file",
		}
	}

	return config, nil
}
