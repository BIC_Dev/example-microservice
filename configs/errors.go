package configs

// ConfigErr represents the structure of a config error
type ConfigErr struct {
	Err error  // Error
	Msg string // Additional context for error
}

// Error returns the string version of the error message
func (ce ConfigErr) Error() string {
	return ce.Err.Error()
}

// Message returns additional context for an error
func (ce ConfigErr) Message() string {
	return ce.Msg
}
