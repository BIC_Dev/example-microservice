module gitlab.com/BIC_Dev/example-microservice

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	go.uber.org/zap v1.16.0
	gopkg.in/yaml.v2 v2.3.0
)
